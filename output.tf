output "public_fqdn-1" {
    value = azurerm_public_ip.linuxlab-1.fqdn
}

output "public_fqdn-2" {
    value = azurerm_public_ip.linuxlab-2.fqdn
}

output "connect_string-1" {
    value = "ssh -i c:\\ssh\\.ssh ${azurerm_linux_virtual_machine.linuxlab-1.admin_username}@${azurerm_public_ip.linuxlab-1.fqdn}"
}

output "connect_string-2" {
    value = "ssh -i c:\\ssh\\.ssh ${azurerm_linux_virtual_machine.linuxlab-2.admin_username}@${azurerm_public_ip.linuxlab-2.fqdn}"
}