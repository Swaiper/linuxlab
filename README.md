# linuxlab for Xplosion AB



## Getting started

First run "Terraform init" to set up the terraform environment. After that run the file "startup.ps1" which will ask you for some input and then run the script setting up two vms, a webserver and a shared disk in the Azure cloud.

## Webserver

The file "index.html" can be swapped for anything you would like the webserver to present as long as it keeps the name.

## Connect strings

After setting everything up, if for whatever reason the terminal clears you can type "terraform ouput" to see connect strings as well as webserver address

## File share

There is a 10GB disk mounted under /mnt/firecracker on the first vm, for this to work you need to restart the second vm with "sudo init 6" and then you can find it under /var/firecracker on the second vm as a shared folder.