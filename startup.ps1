#Skapar ett nytt directory för SSH nyckel
New-Item -ItemType "directory" -Path "c:\ssh"

#Skapar ssh nyckeln under directory C:\ssh\
ssh-keygen -t rsa -b 4096 -f C:\ssh\.ssh -q -N '""'

#Tar ut värden för VMsen
$vm1 = Read-Host "What hostname should the first VM have?"
$vm2 = Read-Host "What hostname should the second VM have?"
$disk = Read-Host "How many gigabytes should the OS-disk for $vm1 be?"
if ($disk -lt 30) {
    Write-Output "Diskspace of $disk GB is too small"
    $disk = Read-Host "How many gigabytes should the OS-disk for $vm1 be?"
}
$disk2 = Read-Host "How many gigabytes should the OS-disk for $vm2 be?"
if ($disk2 -lt 30) {
    Write-Output "Diskspace of $disk2 GB is too small"
    $disk2 = Read-Host "How many gigabytes should the OS-disk for $vm2 be?"
}
$ipad1 = Read-Host "What ipaddress would you like for the first VM? subnet 10.0.2.0/24"
$ipad2 = Read-Host "What ipaddress would you like for the second VM? subnet 10.0.2.0/24"

#Kör terraform för att sätta upp VMsen
terraform apply -var "osdisk=$disk" -var "osdisk2=$disk2" -var "ipaddress1=$ipad1" -var "ipaddress2=$ipad2" -var "vmname1=$vm1" -var "vmname2=$vm2" -auto-approve

#Plockar ut fqdn från connect string
$fqdn_raw = terraform output | Select-String "fqdn"
$fqdn = ($fqdn_raw[0] -split '\s+',4)[2]
$fqdn = $fqdn.Replace('"',"")

#Plockar ut ip addressen för webservern
$dns = Resolve-DnsName "$fqdn"
$webserver = $dns.IPAddress + ":80"
Write-Output "Webserver: $webserver"