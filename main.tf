provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "linuxlab" {
  name     = "linuxlab-resources"
  location = "North Europe"
}

resource "azurerm_virtual_network" "linuxlab" {
  name                = "linuxlab-network"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.linuxlab.location
  resource_group_name = azurerm_resource_group.linuxlab.name
}

resource "azurerm_subnet" "linuxlab" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.linuxlab.name
  virtual_network_name = azurerm_virtual_network.linuxlab.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_network_interface" "linuxlab-nic1" {
  name                = "linuxlab-nic1"
  location            = azurerm_resource_group.linuxlab.location
  resource_group_name = azurerm_resource_group.linuxlab.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.linuxlab.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ipaddress1
    public_ip_address_id          = azurerm_public_ip.linuxlab-1.id
  }
}

resource "azurerm_network_interface" "linuxlab-nic2" {
  name                = "linuxlab-nic2"
  location            = azurerm_resource_group.linuxlab.location
  resource_group_name = azurerm_resource_group.linuxlab.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.linuxlab.id
    private_ip_address_allocation = "Static"
    private_ip_address            = var.ipaddress2
    public_ip_address_id          = azurerm_public_ip.linuxlab-2.id
  }
}

# Generate random string for dns
resource "random_string" "random" {
  length           = 16
  upper            = false
  special          = false
}


# Create public IPs
resource "azurerm_public_ip" "linuxlab-1" {
    name                         = "linuxlabPublicIP-1"
    location                     = azurerm_resource_group.linuxlab.location
    resource_group_name          = azurerm_resource_group.linuxlab.name
    allocation_method            = "Dynamic"
    domain_name_label            = "linux-lab1-dns-${random_string.random.id}"
}

resource "azurerm_public_ip" "linuxlab-2" {
    name                         = "linuxlabPublicIP-2"
    location                     = azurerm_resource_group.linuxlab.location
    resource_group_name          = azurerm_resource_group.linuxlab.name
    allocation_method            = "Dynamic"
    domain_name_label            = "linux-lab2-dns-${random_string.random.id}"
}

# Create Network Security Group and rule
resource "azurerm_network_security_group" "linuxlab-NSG-1" {
    name                = "linuxlab-NSG-1"
    location            = azurerm_resource_group.linuxlab.location
    resource_group_name = azurerm_resource_group.linuxlab.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

     security_rule {
        name                       = "HTTP"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

resource "azurerm_network_security_group" "linuxlab-NSG-2" {
    name                = "linuxlab-NSG-2"
    location            = azurerm_resource_group.linuxlab.location
    resource_group_name = azurerm_resource_group.linuxlab.name

    security_rule {
        name                       = "SSH"
        priority                   = 1001
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "22"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }

     security_rule {
        name                       = "HTTP"
        priority                   = 1002
        direction                  = "Inbound"
        access                     = "Allow"
        protocol                   = "Tcp"
        source_port_range          = "*"
        destination_port_range     = "80"
        source_address_prefix      = "*"
        destination_address_prefix = "*"
    }
}

# Connect the security group to the network interface
resource "azurerm_network_interface_security_group_association" "linuxlab-NSG-1" {
    network_interface_id      = azurerm_network_interface.linuxlab-nic1.id
    network_security_group_id = azurerm_network_security_group.linuxlab-NSG-1.id
}

resource "azurerm_network_interface_security_group_association" "linuxlab-NSG-2" {
    network_interface_id      = azurerm_network_interface.linuxlab-nic2.id
    network_security_group_id = azurerm_network_security_group.linuxlab-NSG-2.id
}

resource "azurerm_linux_virtual_machine" "linuxlab-1" {
  name                = var.vmname1
  resource_group_name = azurerm_resource_group.linuxlab.name
  location            = azurerm_resource_group.linuxlab.location
  size                = "Standard_B1s"
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.linuxlab-nic1.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("C:\\ssh\\.ssh.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
    disk_size_gb         = var.osdisk
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }

  provisioner "file" {
    connection {
      type        = "ssh"
      user        = "adminuser"
      host        = azurerm_public_ip.linuxlab-1.fqdn
      private_key = file("C:\\ssh\\.ssh")
      agent       = false
      timeout     = "2m"
    }
   source      = "index.html"
   destination = "/home/adminuser/index.html"
  }

  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "adminuser"
      host        = azurerm_public_ip.linuxlab-2.fqdn
      private_key = file("C:\\ssh\\.ssh")
      agent       = false
      timeout     = "2m"
    }

    inline = [
      "sudo apt update",
      "sudo apt install nfs-common -y",
      "sudo mkdir /var/firecracker",
      "echo '${var.ipaddress1}:/mnt/firecracker /var/firecracker nfs defaults 0 0' | sudo tee -a /etc/fstab"
    ]
  }
}

resource "azurerm_managed_disk" "nfs" {
  name                 = "nfs-disk"
  location             = azurerm_resource_group.linuxlab.location
  resource_group_name  = azurerm_resource_group.linuxlab.name
  storage_account_type = "Standard_LRS"
  create_option        = "Empty"
  disk_size_gb         = 10
}

resource "azurerm_virtual_machine_data_disk_attachment" "nfs" {
  managed_disk_id    = azurerm_managed_disk.nfs.id
  virtual_machine_id = azurerm_linux_virtual_machine.linuxlab-1.id
  lun                = "10"
  caching            = "ReadWrite"
}

resource "azurerm_linux_virtual_machine" "linuxlab-2" {
  name                = var.vmname2
  resource_group_name = azurerm_resource_group.linuxlab.name
  location            = azurerm_resource_group.linuxlab.location
  size                = "Standard_B1s"
  admin_username      = "adminuser"
  network_interface_ids = [
    azurerm_network_interface.linuxlab-nic2.id,
  ]

  admin_ssh_key {
    username   = "adminuser"
    public_key = file("C:\\ssh\\.ssh.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
    disk_size_gb         = var.osdisk2
  }

  source_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }
  
  provisioner "remote-exec" {
    connection {
      type        = "ssh"
      user        = "adminuser"
      host        = azurerm_public_ip.linuxlab-1.fqdn
      private_key = file("C:\\ssh\\.ssh")
      agent       = false
      timeout     = "2m"
    }

    inline = [
      "sudo apt update",
      "sudo apt install apache2 -y",
      "sudo systemctl enable apache2.service", # Startar apache2 vid reboot
      "sudo mv /home/adminuser/index.html /var/www/html/index.html",
      "sudo systemctl restart apache2",
      "sleep 25",
      "sudo apt install nfs-kernel-server -y",
      "sudo parted /dev/sdc mklabel gpt",
      "sudo parted -a opt /dev/sdc mkpart primary ext4 0% 100%",
      "sleep 10",
      "sudo mkfs.ext4 /dev/sdc1 -F",
      "sleep 10",
      "sudo mkdir /mnt/firecracker",
      "sudo mount /dev/sdc1 /mnt/firecracker",
      "sudo chown nobody:nogroup /mnt/firecracker",
      "sudo chmod 777 /mnt/firecracker",
      "echo '/mnt/firecracker        10.0.2.0/255.255.255.0(rw,sync,no_subtree_check)' | sudo tee -a /etc/exports",
      "sudo exportfs -a",
      "sudo systemctl restart nfs-kernel-server",
      "sudo ufw disable"
    ]
  }
}
