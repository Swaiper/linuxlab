variable "osdisk" {
 type        = string
 description = "disk size of the OS for the first vm"
 default     = "64"
}

variable "osdisk2" {
 type        = string
 description = "disk size of the OS for the second vm"
 default     = "64"
}

variable "ipaddress1" {
 type        = string
 description = "sets the ipaddress of the first vm"
 default     = "10.0.2.10"
}

variable "ipaddress2" {
 type        = string
 description = "sets the ipaddress of the second vm"
 default     = "10.0.2.20"
}

variable "vmname1" {
 type        = string
 description = "sets the hostname of the first vm"
 default     = "linuxlab-1"
}

variable "vmname2" {
 type        = string
 description = "sets the hostname of the second vm"
 default     = "linuxlab-2"
}